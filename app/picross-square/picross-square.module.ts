// Angular Imports
import { NgModule } from '@angular/core';

// This Module's Components
import { PicrossSquareComponent } from './picross-square.component';

@NgModule({
    imports: [

    ],
    declarations: [
        PicrossSquareComponent,
    ],
    exports: [
        PicrossSquareComponent,
    ]
})
export class PicrossSquareModule {

}
